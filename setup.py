# -*- coding: utf-8 -*-
"""
Installation example
"""

from setuptools import setup, find_packages

setup(
    name='mypkg1',
    version='0.0.1',
    author='HydroGeeks',
    description='Do nothing for test',
    license='MIT',
    packages=find_packages(exclude=['build', 'dist', '*.egg-info']),
    classifiers=[
        'Development status :: Only for test',
    ],
    install_requires=[
        'numpy',
        'pandas',
        'matplotlib',
        'toml',
    ]
)
