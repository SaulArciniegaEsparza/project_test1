# -*- coding: utf-8 -*-
"""
Test main class
"""

from time import sleep
from .tools import print_progress

# Do nothing class
class MyClass(object):
    def __init__(self):
        self.a = 0
        self._b = 0

    def do_nothing(self):
        print_progress(0, 50, prefix = 'Progress:', suffix = 'Complete', bar_length = 50)
        for i in range(50):
            sleep(0.1)
            print_progress(i + 1, 50, prefix = 'Progress:', suffix = 'Complete', bar_length = 50)
        print('Nothing was done!')

    def set_a(self, a):
        self.a = a

    def set_b(self, b):
        self._b = b

    def get_a(self):
        return self.a

    def get_b(self):
        return self._b
